<!-- une classe, un rôle.
La class Personnagea pour rôle de représenter un personnage présent en BDD. Elle n'a en aucun cas pour rôle de les gérer. Cette gestion sera le rôle d'une autre classe, communément appelée manager. Dans notre cas, notre gestionnaire de personnage sera tout simplement nommée PersonnagesManager. -->


<?php

class Personnage
{
    private $_id;
    private $_name;
    private $_hp;

    const MOI = 1; // Constante renvoyée par la méthode `frapper` si on se frappe soi-même.
    const PERSONNAGE_TUE = 2; // Constante renvoyée par la méthode `frapper` si on a tué le personnage en le frappant.
    const PERSONNAGE_FRAPPE = 3; // Constante renvoyée par la méthode `frapper` si on a bien frappé le personnage.

    //  implémenter le constructeur pour qu'on puisse directement hydrater notre objet lors de l'instanciation de la classe.???
    public function __construct(array $data)
    {
        $this->hydrate($data);
    }


    public function frapper(Personnage $perso)
    {
        // Avant tout : vérifier qu'on ne se frappe pas soi-même.
        if ($perso->id() == $this->_id) {
            return self::MOI;
            // Si c'est le cas, on stoppe tout en renvoyant une valeur signifiant que le personnage ciblé est le personnage qui attaque.
        }

        // On indique au personnage frappé qu'il doit recevoir des dégâts.
        // Puis on retourne la valeur renvoyée par la méthode : self::PERSONNAGE_TUE ou self::PERSONNAGE_FRAPPE
        return $perso->recevoirDegat();
    }
    // Hydratation
    public function hydrate(array $data)
    {
        foreach ($data as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->$method($value);
            }
        }
    }
    public function recevoirDegat()
    {
        // On augmente de 5 les dégâts.
        $this->_hp += 5;

        // Si on a 100 de dégâts ou plus, la méthode renverra une valeur signifiant que le personnage a été tué.
        if ($this->_degats >= 100) {
            return self::PERSONNAGE_TUE;
        }

        // Sinon, elle renverra une valeur signifiant que le personnage a bien été frappé.
        return self::PERSONNAGE_FRAPPE;
    }


    // on fait GETTERS pour pouvoir les lire 
    public function degats()
    {
        return $this->_hp;
    }
    public function id()
    {
        return $this->_id;
    }

    public function nom()
    {
        return $this->_name;
    }

    // SETTERS pour pouvoir modifier leurs valeurs

    public function setDegats($degats)
    {
        $degats = (int) $degats;
        if ($degats >= 0 && $degats <= 100) {
            $this->_hp = $degats;
        }
    }
    public function setId($id)
    {
        $id = (int) $id;
        if ($id > 0) {
            $this->_id = $id;
        }
    }
    public function setNom($nom)
    {
        if (is_string($nom)) {
            $this->_name = $nom;
        }
    }
}
